##Table of content

1. Introduction
	- Background and meet
	- Presentation of the project
2. Talk about different technologies and project setup
3. Theme creation
	- ~~Child themes~~
	- style.css and screenshot
	- Folder structure
	- Starter themes (Sage, Underscores, Bones, UnderStrap)
4. WordPress/PHP fundamentals
	- Different types of page (front page, home, archive, categories, static, post, etc.)
	- Environement variables
	- [Global variables](https://codex.wordpress.org/Global_Variables)
	- WP_Query
	- [Conditional Tags](https://codex.wordpress.org/Conditional_Tags)
	- Hooks: actions and filters
	- `wp_enqueue_style`, `wp_enqueue_script`
	- Getting data inside/outside the loop with functions
4. Templating:
	- [WordPress template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)
	- Creating page templates
	- Partials and regions: `wp_head`, `wp_footer`
	- includes & get_template_part
	- Navigation Menus
5. Custom Post Types
	- Create custom taxonomy
6. ACF
	- Overview of the plugin and different field types
	- Page Builder
	- Theme Options
	- Repeater
	- Image Gallery
	- Video and oEmbed
	- Google Map
7. AJAX call
7. Blog Posts
	- Category template page
	- Tags
	- [Pagination](https://codex.wordpress.org/Pagination)
8. Backend Admin
	- Register new menus
	- Hide menus
	- Custom stylesheet (login page, color scheme)
8. Useful plugins
	- Contact Form
	- Security
	- SEO
	- Optimization
	- Social Sharing
	- Multilingual (WPML)
	- Migrate DB
	- Dummy content
9. Snippets, tips & tricks
	- Allow upload of SVG
	- Manage body classnames
	- Hide the admin bar when logged in
	- `add_image_size` for custom banners background
	- wysiwyg styles
10. Plugin development, shortcodes, widgets