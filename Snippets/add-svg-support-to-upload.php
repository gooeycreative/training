<?php

/**
 * Allow upload of SVG in the media menu of WordPress
 * @return null
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );