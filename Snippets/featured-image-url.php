<?php
/**
 * Get the featured image's path and echo it as background image
 */
function get_featured_image() {
    $src = '';

    $featured_img_id = get_post_thumbnail_id();
    $featured_img_array = wp_get_attachment_image_src( $featured_img_id, 'background', true );
    $bg_url = $featured_img_array[0];

    if ( has_post_thumbnail() ) {
        return 'style="background-image: url(' . $bg_url . ');"';
    } else {
        return '';
    }
}