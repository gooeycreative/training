<?php

/**
 * Remove the default "Hello Dolly" plugin that comes pre-installed with WP
 * @return none
 */
function goodbye_dolly() {
    if (file_exists(WP_PLUGIN_DIR.'/hello.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('hello.php'));
    }
}
add_action('admin_init','goodbye_dolly');