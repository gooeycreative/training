<?php

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form() {
  $form = '';
  locate_template('/templates/partials/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', 'get_search_form');