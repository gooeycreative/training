<?php

/**
 * Register the Options page in the Admin
 */
function theme_options_page() {
    if ( function_exists('acf_add_options_page') ) {
        acf_add_options_page( array(
            'page_title'    => __('Theme Options', TEXT_DOMAIN),
            'menu_title'    => __('Theme Options', TEXT_DOMAIN),
            'menu_slug'     => 'theme_options',
            'parent_slug'   => '',
            'capability'    => 'manage_options',
            'position'      => 79,
            'icon_url'      => 'dashicons-admin-generic',
        ));
    }
}
add_action('init', 'theme_options_page');