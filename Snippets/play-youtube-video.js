$( '.btn-play' ).on( 'click', function(e) {
    var $this = $( e.target );
    var $player = $this.closest( '.section' ).find( 'iframe' );
    var iframe_url = $player[0].src;

    // Fadeout the play button
    $player.fadeIn( 500, function() {
        // Unbind the click event
        $this.off( 'click' );
    });

    // Add the autoplay param in the ifram URL to auto start it
    if ( iframe_url && iframe_url.indexOf('?') > 1 ) {
        $player[0].src += '&autoplay=1';
    } else {
        $player[0].src += '?autoplay=1';
    }
});