<?php
// Place this code inside the "single.php" template file at the root of the
// theme folder. It will dynamically load the "content-single-{type}" file
// inside the "templates" folder where {type} will be the custom post type
//
// For example, "templates/content-single-career" would be loaded when accessing
// the detail page of a "career" post.
get_template_part('templates/content-single', get_post_type()); ?>