<div class="container">
    <?php $locations = get_terms( 'career_location'); ?>
    <?php echo 'GET = ' . $_GET['cat']; ?>

    <form method="get">
        <select name="cat">
            <option value="">All locations</option>

            <?php foreach ( $locations as $location ) : ?>
                <option value="<?php echo $location->term_id; ?>" <?php if ( $_GET['cat'] && $_GET['cat'] == $location->term_id ) echo 'selected'; ?>>
                    <?php echo $location->name; ?>
                </option>
            <?php endforeach; ?>
        </select>

        <button type="submit">Filter</button>
    </form>
</div>

<div class="container">
    <div class="row">
        <?php
        $args = array(
            'posts_per_page'    => -1,
            'post_type'         => 'career',
            'post_status'       => 'publish',
        );

        if ( $_GET['cat'] ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'career_location',
                    'field'    => 'term_id',
                    'terms'    => $_GET['cat'],
                ),
            );
        }

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <?php get_template_part('templates/posts/career-preview'); ?>
                </div>
            <?php endwhile; ?>
        <?php else : ?>
            <?php _e( 'No results found.', TEXT_DOMAIN ); ?>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
    </div>
</div>
