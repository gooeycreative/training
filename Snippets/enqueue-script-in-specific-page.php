<?php

// Enqueue a specific script only in the "Contact" page
if ( is_page_template('template-contact.php') ) {
    wp_enqueue_script( 'theme_modernizr', get_template_directory_uri() . '/assets/js/contact.js', array(), null, false );
}