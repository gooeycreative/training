<?php
// This will output the  post categories as hyperlinks
$categories = get_categories();

foreach ($categories as $cat) {
   $category_link = get_category_link($cat->cat_ID);
   echo '<a href="<?php echo esc_url( $category_link ); ?>" title="Category Name"><?php echo $cat->name; ?></a>';
}