<?php

/**
 * Set a custom excerpt length
 */
function excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length',  'excerpt_length' );