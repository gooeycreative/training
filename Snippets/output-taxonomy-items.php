<?php $career_locations = get_terms('career_location'); ?>
<ul>
    <?php foreach ( $career_locations as $location ) : ?>
    <li class="cat">
        <a href="#" data-id="<?php echo $location->term_id; ?>"><?php echo $location->name; ?></a>
    </li>
    <?php endforeach; ?>
</ul>