<?php
function remove_editor_menu() {
    // Hide the comments menu in the backend
    remove_menu_page( 'edit-comments.php' );

    // Disable Theme Editor
    remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);