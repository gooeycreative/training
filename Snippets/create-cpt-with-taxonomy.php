<?php
// Create Custom Post Type
function register_careers_posttype() {
    $labels = array(
        'name'              => _x( 'Careers', 'post type general name' ),
        'singular_name'     => _x( 'Careers', 'post type singular name' ),
        'add_new'           => _x( 'Add New', 'career' ),
        'add_new_item'      => __( 'Add New Career' ),
        'edit_item'         => __( 'Edit Career' ),
        'new_item'          => __( 'New Career' ),
        'view_item'         => __( 'View Career' ),
        'search_items'      => __( 'Search Careers' ),
        'not_found'         => __( 'No careers found' ),
        'not_found_in_trash'=> __( 'No careers found in Trash' ),
        'parent_item_colon' => __( 'Career' ),
        'menu_name'         => __( 'Careers' )
    );

    $taxonomies = array();

    $post_type_args = array(
        'labels'            => $labels,
        'singular_label'    => __( 'Career' ),
        'public'            => true,
        'show_ui'           => true,
        'show_in_menu' 		=> true,
        'publicly_queryable'=> true,
        'query_var'         => true,
        'capability_type'   => 'post',
        'has_archive'       => false,
        'hierarchical'      => false,
        'rewrite'           => array( 'slug' => 'careers-list', 'with_front' => false ),
        'menu_position'     => 27, // Where it is in the menu. Change to 6 and it's below posts. 11 and it's below media, etc.
        'menu_icon'         => 'dashicons-welcome-learn-more',
        'taxonomies'        => $taxonomies,
        'supports' 			=> array('title', 'editor')
    );
    register_post_type( 'career', $post_type_args );
}
add_action( 'init', 'register_careers_posttype' );


function career_taxonomies() {
    $labels = array(
        'name'              => _x( 'Career Locations', 'taxonomy general name' ),
        'singular_name'     => _x( 'Career Location', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Career Locations' ),
        'all_items'         => __( 'All Career Locations' ),
        'parent_item'       => __( 'Parent Location' ),
        'parent_item_colon' => __( 'Parent Location:' ),
        'edit_item'         => __( 'Edit Career Location' ),
        'update_item'       => __( 'Update Career Location' ),
        'add_new_item'      => __( 'Add New Career Location' ),
        'new_item_name'     => __( 'New Career Location' ),
        'menu_name'         => __( 'Career Locations' ),
    );

    $args = array(
        'labels'            => $labels,
        'show_in_nav_menus' => true,
        'hierarchical'      => true,
        'public'            => true,
        'publicly_queryable'=> true,
        'show_in_menu'      => true,
        'show_admin_column' => true,
        'show_in_quick_edit'=> true,
        'rewrite' => array( 'slug' => _x('career-locations', 'URL Slug', TEXT_DOMAIN) ),
    );
    register_taxonomy( 'career_location', 'career', $args );
}
add_action( 'init', 'career_taxonomies', 0 );
