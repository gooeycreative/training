<?php

// Hide the WP Admin Bar on the public site when logged in
add_filter( 'show_admin_bar', '__return_false' );