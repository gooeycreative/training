<?php if ( have_rows('content_sections') ) :
    while ( have_rows('content_sections') ) : the_row();

        // Section - General content
        if ( get_row_layout() == 'section_general' ) :
            get_template_part('templates/sections/section-general');
        endif;

        // Section - Split Columns
        if ( get_row_layout() == 'section_split' ) :
            get_template_part('templates/sections/section-split');
        endif;

        // Section - Gallery
        if ( get_row_layout() == 'section_gallery' ) :
            get_template_part('templates/sections/section-gallery');
        endif;

        // Section - Contact
        if ( get_row_layout() == 'section_contact' ) :
            get_template_part('templates/sections/section-contact');
        endif;

        // Section - Carousel
        if ( get_row_layout() == 'section_carousel' ) :
            get_template_part('templates/sections/section-carousel');
        endif;

    endwhile;
endif; ?>