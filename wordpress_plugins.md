* [Synchronize Databases - WP Migrate DB Pro](https://deliciousbrains.com/wp-migrate-db-pro/) **paid**
* Forms: Contact Form 7
* SEO: Yoast SEO
* Security: Askismet, WordFence
* Optimization: WP Smush
* Social Sharing: Sassy Social Share
* Multilingual: WPML